I'm proud to make the following services add free and track free, but if you find it useful, and if you'd like to support the development and maintenance, you can:


- Just offer me matikum lunch next time you cross me in Uppsala
- Answer this [form](https://framaforms.org/pick-next-project-1669578695)
- Send a [Yocha gift card](https://www.yochastudio.com/gift-card) (CB, Swish) to mardubr-site@yahoo.com

And if you let me know for which project you give then I will prioritize the maintenance of it.

---
Project list can be found on:
[http://www.mariedubremetz.com](http://www.mariedubremetz.com)
among others there is [Uppsala Women Coding](https://www.meetup.com/fr-FR/Uppsala-women-coding-beginners-welcome/), [beer](https://mardub.gitlab.io/beer/), [chiasmus](https://cl.lingfil.uu.se/~marie/cgi/demo.html), [foodblog](https://food.uppsala.ai), [song](https://mardub.gitlab.io/song/), ...


|Date        |Who     |Method |value    |For project      |
|------------|--------|-------|---------|-----------------|
|- April 2021| Henric | Lunch | 100 SEK | lunch.uppsala.ai|

